# smolpxl-website

The code behind https://smolpxl.artificialworlds.net .

Code for the games and the library that is used to make them is at
https://gitlab.com/smolpxl/smolpxl .

## Popularity tracking

So we can see how popular the games are, we collect anonymous popularity
statistics.

We send metrics from JavaScript (in the smolpxl code) by making POST requests
that are processed by [event.php](smolpxl.artificialworlds.net/s/event.php).

We aggregate metrics via a [cron job](cronjobs.md) that runs
[aggregate.php](scripts/aggregate.php).

We store metrics in MySQL [tables.md](tables.md).

We present metrics using an install of [grafana](grafana.md).

We deploy some of this manually, and some of it using the [Makefile](Makefile).

## License and code of conduct

Copyright 2020 Andy Balaam and contributors, released under the
[AGPLv3 license](LICENSE) or later.

Contains icons from the
[Feather Icons](https://github.com/feathericons/feather) set, which are
Copyright 2013-2017 Cole Bemis, and released under the
[MIT License](https://github.com/feathericons/feather/blob/8b5d6802fa8fd1eb3924b465ff718d2fa8d61efe/LICENSE).

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md).  By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](images-src/contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)
