<?php
include_once '../../scripts/config.php';

header("Access-Control-Allow-Origin: *");

$metric = trim(file_get_contents("php://input"));

if ($metric === '') {
    http_response_code(400);
    print 'Metric name must not be empty';
    return;
}

if (!preg_match('/^[a-z0-9._-]+$/', $metric)) {
    http_response_code(400);
    print 'Metric name must ONLY use these characters: "a-z0-9._-"';
    return;
}

$db = new PDO(
    "mysql:host=$config[mysql_host];dbname=$config[mysql_name]",
    $config['mysql_user'],
    $config['mysql_password']
);

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$stmt = $db->prepare(
    'INSERT INTO events (time, metric) ' .
    'VALUES (' .
        'UNIX_TIMESTAMP(), ' .
        "?" .
    ')'
);

$stmt->execute([$metric]);

http_response_code(204);
