<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Smolpxl Games</title>
<link href="style.css" rel="stylesheet">
<style>
main {
    color: black;
    text-align: center;
}
section {
    display: inline-block;
    margin: 1em;
}
section > div {
    display: flex;
    flex-direction: column;
}
section p {
    text-align: center;
    margin: 0em;
    padding: 0.1em 0.2em;
    width: 100%;
}
section p a {
    background-color: #55dde0;
    color: black;
    display: inline-block;
    font-size: 95%;
    padding: 0.2em 0.4em;
    border-radius: 0.3em;
    border: 0.15em solid #2f4858;
    box-shadow: 0.1em 0.1em 0.2em rgba(0, 0, 0, .3);
}
section p a.play {
    background-color: #eeeeee;
}
section p a.play:hover {
    background-color: #ffffff;
}
section p a span {
    display: flex;
    align-items: center;
}
section p a span img {
    height: 0.9em;
    width: 0.9em;
    margin-left: 0.3em;
}
section p a span span {
    margin-bottom: -0.15em;
}
section p a:hover {
    background-color: #61fcff;
    box-shadow: 0.2em 0.2em 0.4em rgba(0, 0, 0, .4);
}
section .image {
    background-size: contain;
    background-repeat: no-repeat;
    border-radius: 2em;
    border: 0.3em solid #2f4858;
    margin-bottom: 0.1em;
    width: 20em;
    height: 21.1em;
}
section.text .image {
    background-color: #f3ffbd;
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 0.6em;
}
section.text .image p {
    margin-bottom: 0.5em;
}
section.text:hover .image {
    background-color: #ffffff;
}
section.text .image strong {
    color: #0e649b;
}
<?php
foreach ($games as $id => $game) {
?>
    section#<?= $id ?> .image {
            background-image: url('images/<?= $id ?>.png');
    }
    section#<?= $id ?>:hover .image {
        background-image: url('images/<?= $id ?>.gif');
    }
<?
}
?>
</style>
<script>
function statsEvent(metric) {
    var req = new XMLHttpRequest();
    req.open("POST", "https://smolpxl.artificialworlds.net/s/event.php", true);
    req.setRequestHeader("Content-Type", "text/plain; charset=UTF-8");
    req.send(metric);
}
function like(name) {
    statsEvent(`${name}.liked`);
    document.getElementById(`like-${name}-img`).src = "images/liked.svg";
}
window.addEventListener('load', function() {
    statsEvent("index.opened");
    for (const el of document.getElementsByClassName("opena")) {
        el.addEventListener(
            "click", () => statsEvent(`${el.dataset.id}.opened`));
    }
});
</script>
</head>
<body>
<header>
    <h1><a href="/">Smolpxl Games</a></h1>
    <a class="firstMenuItem" href="privacy.html">Privacy</a>
    <a class="menuItem" href="safety.html">Safety</a>
    <a class="menuItem" href="https://lemmy.ml/c/smolpxl">Community</a>
    <a class="menuItem" href="donate.html">Donate</a>
    <a class="menuItem" href="makeagame.html">Make a game</a>
    <a class="menuItem" href="contact.html">Contact</a>
</header>
<main>
<?php
$counter = 0;

$c = count($games) - 1;
$vote_loc = rand(7, $c);
$donate_loc = rand(7, $c);
$makeagame_loc = rand(7, $c);

foreach ($games as $id => $game) {

if ($counter == $vote_loc) {
?>
    <section class="text"><div>
        <a href="https://lemmy.ml/post/55625">
            <h2>Vote</h2>
            <div class="image">
                <p>Go to the <strong>What shall we add?</strong> post in our
                Lemmy community!</p>
                <p>Vote or discuss what new games you would like to see.</p>
                <p>Suggest new levels or features.</p>
                <p>We really want to hear from you!</p>
            </div>
        </a>
        <p>Vote for what we do!</p>
        <p>
            <a
                href="https://lemmy.ml/post/55625"
                class="play"
            >
                <span><span>Vote </span><img src="images/vote.svg"/></span>
            </a>
        </p>
    </div></section>
<?
}

if ($counter == $donate_loc) {
?>
    <section class="text"><div>
        <a href="donate.html">
            <h2>Donate</h2>
            <div class="image">
                <p>Smolpxl is made for fun, by volunteers.</p>
                <p>We don't track you or try to sell you anything.</p>
                <p>We just want you to have fun playing our games, and maybe
                learn how to make games yourself!</p>
                <p>If you'd like to see more games and tutorials,
                or you'd like to help with the costs of running the site,
                you can find out how to donate by clicking here.</p>
            </div>
        </a>
        <p>Donate to help us make games</p>
        <p>
            <a href="donate.html" class="play">
                <span><span>Donate </span><img src="images/donate.svg"/></span>
            </a>
        </p>
    </div></section>
<?
}

if ($counter == $makeagame_loc) {
?>
    <section class="text"><div>
        <a href="makeagame.html">
            <h2>Make a game</h2>
            <div class="image">
                <p>You can make a game too!</p>
                <p>Learn how to code games in JavaScript.</p>
                <p>Watch videos on how to make a Smolpxl game.</p>
                <p>Click here to get started!</p>
            </div>
        </a>
        <p>Learn how to code games</p>
        <p>
            <a href="makeagame.html" class="play">
                <span><span>Create a game </span><img src="images/makeagame.svg"/></span>
            </a>
        </p>
    </div></section>
<?
}

++$counter;
?>
    <section id="<?= $id ?>"><div>
        <a href="<?= $game['url'] ?>" class="opena" data-id="<?= $id ?>">
            <h2><?= $game['name'] ?></h2>
            <div class="image"></div>
        </a>
        <p>Plays:<?= $game['played'] ?> &nbsp; Likes:<?= $game['liked'] ?></p>
        <p>
            <a
                href="<?= $game['url'] ?>"
                class="play opena"
                data-id="<?= $id ?>"
            >
                <span>
                    <span>Play </span>
                    <img src="images/play.svg"/>
                </span>
            </a>
            <a href="javascript:like('<?= $id ?>');" class="play">
                <span>
                    <span>Like </span>
                    <img id="like-<?= $id ?>-img" src="images/like.svg"/>
                </span>
            </a>
            <? if ($game['sourceCodeUrl']) { ?>
                <a
                    href="<?= $game['sourceCodeUrl'] ?>"
                    class="play"
                    target="_blank"
                >
                    <span>
                        <span>Code </span>
                        <img src="images/code.svg"/>
                    </span>
                </a>
            <? } ?>
        </p>
    </div></section>
<?
}
?>
</main>
<footer>
    All the Smolpxl games are Free and Open Source Software!  Get the code
    and learn how to make your own games at
    <a href="https://gitlab.com/smolpxl/smolpxl"
    >gitlab.com/smolpxl/smolpxl</a>.
</footer>
</body>
</html>

