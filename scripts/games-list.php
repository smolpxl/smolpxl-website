<?php
$games = array(
    "box-stacker" => array(
        "name" => "Box Stacker",
        "url" => "https://box-stacker.artificialworlds.net/game/",
        "sourceCodeUrl" => "https://gitlab.com/box-stacker/box-stacker",
    ),
    "gonk-winter-olympics" => array(
        "name" => "Gonk Winter Olympics",
        "url" => "https://gonk-olympics.artificialworlds.net/#winter",
        "sourceCodeUrl" => "https://codeberg.org/andybalaam/gonk-olympics",
    ),
    "rightwaves" => array(
        "name" => "Rightwaves",
        "url" => "https://smolpxl.gitlab.io/rightwaves/",
        "sourceCodeUrl" => "https://gitlab.com/smolpxl/rightwaves",
    ),
    "gonk-olympics" => array(
        "name" => "Gonk Olympics",
        "url" => "https://gonk-olympics.artificialworlds.net/",
        "sourceCodeUrl" => "https://codeberg.org/andybalaam/gonk-olympics",
    ),
    "spring" => array(
        "name" => "Spring",
        "url" => "https://smolpxl.gitlab.io/smolpxl/spring",
        "sourceCodeUrl" => "https://gitlab.com/smolpxl/smolpxl/-/blob/master/public/spring/game.js",
    ),
    "santabike" => array(
        "name" => "Santa Bike",
        "url" => "https://santabike.artificialworlds.net/",
        "sourceCodeUrl" => "https://github.com/CodeSmith00/SantaBike",
    ),
    "eatapplesquick" => array(
        "name" => "Eat Apples Quick!",
        "url" => "https://smolpxl.gitlab.io/eat-apples-quick/",
        "sourceCodeUrl" => "https://gitlab.com/smolpxl/eat-apples-quick",
    ),
    "tron" => array(
        "name" => "Tron",
        "url" => "https://smolpxl.gitlab.io/tron/",
        "sourceCodeUrl" => "https://gitlab.com/smolpxl/tron",
    ),
    "duckmaze2" => array(
        "name" => "Duckmaze 2",
        "url" => "https://smolpxl.gitlab.io/smolpxl/duckmaze2",
        "sourceCodeUrl" => "https://gitlab.com/smolpxl/smolpxl/-/blob/master/public/duckmaze2/game.js",
    ),
    "cross-the-road" => array(
        "name" => "Cross the Road",
        "url" => "https://andybalaam.gitlab.io/cross-the-road/",
        "sourceCodeUrl" => "https://gitlab.com/andybalaam/cross-the-road",
    ),
    "tunnel" => array(
        "name" => "Tunnel",
        "url" => "https://andybalaam.gitlab.io/tunnel/",
        "sourceCodeUrl" => "https://gitlab.com/andybalaam/tunnel/",
    ),
    "snake" => array(
        "name" => "Snake",
        "url" => "https://smolpxl.gitlab.io/smolpxl/snake",
        "sourceCodeUrl" => "https://gitlab.com/smolpxl/smolpxl/-/blob/master/public/snake/game.js",
    ),
    "color-game" => array(
        "name" => "Color Game",
        "url" => "https://thejrzombie.itch.io/color-game-1",
        "sourceCodeUrl" => "https://github.com/tHEjrZombie/ColorGame",
    ),
    "heli" => array(
        "name" => "heli",
        "url" => "https://smolpxl.gitlab.io/smolpxl/heli",
        "sourceCodeUrl" => "https://gitlab.com/smolpxl/smolpxl/-/blob/master/public/heli/game.js",
    ),
    "life" => array(
        "name" => "Conway's Game of Life",
        "url" => "https://smolpxl.gitlab.io/smolpxl/life",
        "sourceCodeUrl" => "https://gitlab.com/smolpxl/smolpxl/-/blob/master/public/life/game.js",
    ),
);
