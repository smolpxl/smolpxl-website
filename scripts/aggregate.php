#!/usr/local/bin/php-7.4
<?php
include_once 'config.php';

$start = microtime(true);

$db = new PDO(
    "mysql:host=$config[mysql_host];dbname=$config[mysql_name]",
    $config['mysql_user'],
    $config['mysql_password']
);

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$times = array();

function db_exec($query) {
    GLOBAL $db;
    GLOBAL $times;
    $estart = microtime(true);
    $db->exec($query);
    $eend = microtime(true);
    $t = number_format($eend - $estart, 2);
    array_push($times, "$t $query");
}

// Minutes for 2 hours ago -> 2 minutes ago
db_exec(
    'REPLACE INTO stats_per_minute (time, metric, value) ' .
    'SELECT (time DIV 60) * 60 as tmin, metric, COUNT(metric) ' .
    'FROM events ' .
    'WHERE time ' .
        'BETWEEN (((UNIX_TIMESTAMP() - 7200) DIV 60) * 60) ' .
        'AND     (((UNIX_TIMESTAMP() - 120)  DIV 60) * 60) ' .
    'GROUP BY tmin, metric'
);

// Hours for 3 hours ago -> before start of this hour
db_exec(
    'REPLACE INTO stats_per_hour (time, metric, value) ' .
    'SELECT (time DIV 3600) * 3600 as thour, metric, SUM(value) ' .
    'FROM stats_per_minute ' .
    'WHERE time ' .
        'BETWEEN (((UNIX_TIMESTAMP() - 10800) DIV 3600) * 3600) ' .
        'AND     (((UNIX_TIMESTAMP()        ) DIV 3600) * 3600) ' .
    'GROUP BY thour, metric'
);

// Days for 3 days ago -> before start of this day
db_exec(
    'REPLACE INTO stats_per_day (time, metric, value) ' .
    'SELECT (time DIV 86400) * 86400 as tday, metric, SUM(value) ' .
    'FROM stats_per_hour ' .
    'WHERE time ' .
        'BETWEEN (((UNIX_TIMESTAMP() - 259200) DIV 86400) * 86400) ' .
        'AND     (((UNIX_TIMESTAMP()         ) DIV 86400) * 86400) ' .
    'GROUP BY tday, metric'
);

// Delete old rows
db_exec(
    'DELETE FROM events ' .
    'WHERE time < (UNIX_TIMESTAMP() -       31*24*60*60)'
);
db_exec(
    'DELETE FROM stats_per_minute ' .
    'WHERE time < (UNIX_TIMESTAMP() -     3*31*24*60*60)'
);
db_exec(
    'DELETE FROM stats_per_hour ' .
    'WHERE time < (UNIX_TIMESTAMP() -    12*31*24*60*60)'
);
db_exec(
    'DELETE FROM stats_per_day ' .
    'WHERE time < (UNIX_TIMESTAMP() - 50*12*31*24*60*60)'
);

// Log if we were slow
$t = microtime(true) - $start;

if ($t > 1.0) {
    $time_elapsed_secs = number_format($t, 2);
    print "Adding $count aggregates took $time_elapsed_secs seconds.\n\n";

    foreach ($times as $t) {
        print "$t\n";
    }
}
