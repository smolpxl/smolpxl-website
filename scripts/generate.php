#!/usr/local/bin/php-7.4
<?php
include_once 'config.php';
include_once 'games-list.php';

$start = microtime(true);

function startsWith($haystack, $needle) {
    return (
        strlen($haystack) >= strlen($needle) &&
        substr_compare($haystack, $needle, 0, strlen($needle)) === 0
    );
}

function endsWith($haystack, $needle) {
    return (
        strlen($haystack) >= strlen($needle) &&
        substr_compare($haystack, $needle, -strlen($needle)) === 0
    );
}

function add_stat(&$games, $metric, $type, $value) {
    $game = substr($metric, 0, strlen($metric) - (strlen($type) + 1));
    // Ignore stats about games that are not in our list
    if (array_key_exists($game, $games)) {
        $old_sum = $games[$game][$type] ?? 0;
        $games[$game][$type] = $old_sum + $value;
        //error_log("$game $type += $value");
    } else {
        // Special case for gonk olympics games because they have lots of
        // subgames that we track.
        if (!startsWith($game, "gonk-")) {
            error_log("Found metric $metric, but there is no $game in \$games\n");
        }
    }
}

function add_stats($row, &$games) {
    $metric = $row['metric'];
    $value = $row['value'];
    if (endsWith($metric, '.played')) {
        add_stat($games, $metric, "played", $value);
    } else if (endsWith($metric, '.liked')) {
        add_stat($games, $metric, "liked", $value);
    }
}

if (!isset($games)) {
    throw Exception("No \$games variable set by games-list.php.");
}

$db = new PDO(
    "mysql:host=$config[mysql_host];dbname=$config[mysql_name]",
    $config['mysql_user'],
    $config['mysql_password']
);

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// Gather stats from all days before today
$stmt = $db->query(
    'SELECT metric, SUM(value) AS value ' .
    'FROM stats_per_day '.
    'GROUP BY metric'
);
foreach ($stmt as $row) {
    add_stats($row, $games);
}
$stmt->closeCursor();

// Plus all hours of this day before this hour
$stmt = $db->query(
    'SELECT metric, SUM(value) AS value ' .
    'FROM stats_per_hour '.
    'WHERE time >= ((UNIX_TIMESTAMP() DIV 86400) * 86400)' .
    'GROUP BY metric'
);
foreach ($stmt as $row) {
    add_stats($row, $games);
}
$stmt->closeCursor();

// Plus all minutes of this hour
$stmt = $db->query(
    'SELECT metric, SUM(value) AS value ' .
    'FROM stats_per_minute '.
    'WHERE time >= ((UNIX_TIMESTAMP() DIV 3600) * 3600)' .
    'GROUP BY metric'
);
foreach ($stmt as $row) {
    add_stats($row, $games);
}
$stmt->closeCursor();

include 'index.php';

// Log if we were slow
$t = microtime(true) - $start;

if ($t > 0.2) {
    $time_elapsed_secs = number_format($t, 2);
    error_log("Generating web site took $time_elapsed_secs seconds.\n");
}
