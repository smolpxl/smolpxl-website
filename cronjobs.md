# Cron jobs

To transform events into time series, every hour, we run:

```bash
/home/smolpxl/scripts/aggregate.php
```

To generate the web site, every day, we run:

```bash
/home/smolpxl/scripts/gen_site
```
