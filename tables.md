# MySQL tables

## Events

When metrics are posted, they are stored in the events (transactional) table.
This table is the definitive information store, since it contains the exact
data.

It looks like this:

```sql
CREATE TABLE events (
    id INT AUTO_INCREMENT UNIQUE,
    time INT,
    metric VARCHAR(255) NOT NULL,
    PRIMARY KEY (time, metric, id)
);
```

`timestamp` is seconds since 1970-01-01 00:00 UTC that the event happened.
`metric_id` identifies what happened.

## Time series (aggregates)

Grafana reads time series tables that are generated as a aggregates of the
transactional table.  These tables are in a format that Grafana expects.

They look like this:

```sql
CREATE TABLE stats_per_minute (
    time INT,
    metric VARCHAR(255) NOT NULL,
    value INT,
    PRIMARY KEY (time, metric)
);
```

```sql
CREATE TABLE stats_per_hour (
    time INT,
    metric VARCHAR(255) NOT NULL,
    value INT,
    PRIMARY KEY (time, metric)
);
```

```sql
CREATE TABLE stats_per_day (
    time INT,
    metric VARCHAR(255) NOT NULL,
    value INT,
    PRIMARY KEY (time, metric)
);
```

`time` is seconds since 1970-01-01 00:00 UTC, but all values are rounded down
to the beginning of a minute/hour/day depending on the table.

`metric` is the name of the game and what happened e.g. `spring.played`.

* "played" means how many people played this game in this minute.
* "opened" means how many people clicked on this game on the main site.
* "liked" means how many people clicked "Like" on this game.

## Data pruning

Data is deleted from each table when it becomes old:

* `events` rows are deleted after 1 month.
* `stats_per_minute` rows are deleted after 3 months.
* `stats_per_hour` rows are deleted after 1 year.
* `stats_per_day` rows are deleted after 50 years.
