# Installing Grafana

I view the smolpxl stats using an install of grafana in my personal web space.

## Web space config

I set up a proxy from smolpxlgrafana.artificialworlds.net to port 9333,
and set up an HTTPS certificate for that domain.

## Install

```bash
ssh smolpxl@smolxpl.artificialworlds.net
wget https://dl.grafana.com/oss/release/grafana-7.1.5.linux-amd64.tar.gz
tar -xf grafana-7.1.5.linux-amd64.tar.gz
```

## Config

```bash
cd ~/grafana-7.1.5
cp conf/sample.ini conf/custom.ini
vim conf/custom.ini
```

Change the following entries:

```
[server]
http_port = 9333
enable_gzip = true

[database]
type = mysql
host = blah
name = blah
user = blah
password = blah

[analytics]
reporting_enabled = false

[users]
allow_sign_up = false
```

## Run

```bash
cd ~/grafana-7.1.5
screen
./bin/grafana-server web
```

Then `Ctrl-a d` to detach from screen.

To get back into the screen session, type:

```bash
screen -r
```

## Login

Go to https://smolpxlgrafana.artificialworlds.net

Initial password was admin/admin but I changed it.
