USER := smolpxl
SERVER := smolpxl.artificialworlds.net

all:
	@echo 'Try "make deploy"'

# "deploy" uploads the PHP+HTML etc. for the web site and stats collection.
# To fully recreate the setup from scratch, you also need to:
# * Set up web space as in README.md
# * create the MySQL DB and tables: tables.md
# * Set up Grafana: grafana.md
# * Create a file scripts/config.php similar to scripts/example-config.php
# * Set up cron jobs as in cronjobs.md
deploy:
	rsync -r ./$(SERVER)/ $(USER)@$(SERVER):$(SERVER)/
	rsync -r ./scripts/ $(USER)@$(SERVER):scripts/
	ssh $(USER)@$(SERVER) /home/smolpxl/scripts/gen_site
